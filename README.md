# exophora_estimator

This repository is to estimate pointing target object by using three probabilistic estimator.  
In additon, this package is based on ROS (Noetic version).  
*   Maintainer: Akira Oyama ([oyama.akira@em.ci.ritsumei.ac.jp](mailto:oyama.akira@em.ci.ritsumei.ac.jp)).
*   Author: Akira Oyama ([oyama.akira@em.ci.ritsumei.ac.jp](mailto:oyama.akira@em.ci.ritsumei.ac.jp)).

**Content:**

*   [Setup](#Setup)
*   [Launch](#launch)
*   [Files](#files)
*   [References](#References)


## Setup
`pip install requirements.txt`  
(Since library `opencv-python` and `opencv-contrib-python` conflict, delete `opencv-python`.)

In additon, you need to prepare the following program to create dataset.  
* [yolov5-ros](https://gitlab.com/general-purpose-tools-in-emlab/object-detector/yolo/yolov5-ros): Object detector based on ROS.  
* [cv_get_object_position](https://gitlab.com/general-purpose-tools-in-emlab/data-collection-tools/cv_get_object_position): Data collection of object 3d positions.  
* [cv_get_object_confidence](https://gitlab.com/general-purpose-tools-in-emlab/data-collection-tools/cv_get_object_confidence): Data collection of object confidence.  


## Launch
You can execute this program by using a following command.
~~~
python exophora_estimator.py
~~~

In additon, you can visualize pose landmark model by mediapipe.  
~~~
python output_pose_landmark.py
~~~


## Files
 - `README.md`: Read me file (This file)

 - `__init__.py`

 - `exophora_estimator.py`: main program

 - `output_pose_landmark.py`: output pose landmark model

 - `calculate_poitning_vector.py`

 - `dataset.py`

 - `demonstrative_region_estimator.py`

 - `get_pose_landmark.py`

 - `output_graph.py`

 - `pointing_estimator.py`

 - `visualize_poitning_trajectory.py`


## References

