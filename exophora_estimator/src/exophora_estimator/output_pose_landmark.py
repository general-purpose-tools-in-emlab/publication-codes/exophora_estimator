#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
from sensor_msgs.msg import CompressedImage, Image
import cv2
from cv_bridge import CvBridge
from sensor_msgs.msg import PointCloud2
import math
import tf
from tf import TransformListener
import tf2_ros
import tf2_geometry_msgs
# from std_msgs.msg import String, Header

import mediapipe as mp
mp_holistic = mp.solutions.holistic
mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose

# from geometry_msgs.msg import Pose, PoseStamped
from geometry_msgs.msg import Pose, PoseStamped, Point
from visualization_msgs.msg import Marker, MarkerArray
from ros_numpy.point_cloud2 import pointcloud2_to_xyz_array

class OutputPoseLandmark():
    def __init__(self):

        self.cv_bridge = CvBridge()

        self.keypoint_pub = rospy.Publisher("keypoint_pub", MarkerArray, queue_size = 1)
        self.landmark_line_pub = rospy.Publisher("landmark_line_pub", MarkerArray, queue_size = 1)

        self.subscriber_for_rgb_image = rospy.Subscriber(
            "/hsrb/head_rgbd_sensor/rgb/image_raw/compressed",
            CompressedImage,
            self.image_callback,
            queue_size=1
        )

        self.subscriber_for_point_cloud = rospy.Subscriber(
            "/hsrb/head_rgbd_sensor/depth_registered/rectified_points",
            PointCloud2,
            self.point_cloud_callback,
            queue_size=1
        )

        self.img_pub = rospy.Publisher("/annotated_msg/compressed", CompressedImage, queue_size=1)

        self.mp_pose = mp.solutions.pose
        self.mp_drawing = mp.solutions.drawing_utils
        self.mesh_drawing_spec = self.mp_drawing.DrawingSpec(thickness=2, color=(0, 255, 0))
        self.mark_drawing_spec = self.mp_drawing.DrawingSpec(thickness=10, circle_radius=5, color=(0, 0, 255))

        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
        

    def output_pose_landmark(self):
        aaa = 0
        while not rospy.is_shutdown():
            try:
                height, width, channels = self.rgb_img.shape[:3]
                # ここからmediapipe追加部分
                # 検出結果を別の画像名としてコピーして作成
                self.annotated_image = self.rgb_img.copy()
                with self.mp_pose.Pose(static_image_mode=True, min_detection_confidence=0.5) as pose:
                    results = pose.process(self.rgb_img)
                    # 画像にポーズのランドマークを描画
                    annotated_image = self.rgb_img.copy()
                    # upper_body_onlyがTrueの時
                    # 以下の描画にはmp_pose.UPPER_BODY_POSE_CONNECTIONSを使用
                    mp_drawing.draw_landmarks(annotated_image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS, landmark_drawing_spec=self.mark_drawing_spec)
                    # 色変換
                    # annotated_image = cv2.cvtColor(annotated_image, cv2.COLOR_BGR2RGB)
                    # cv2.imwrite('../data/annotated_image_rgb.png', annotated_image)
                    annotated_msg = self.cv_bridge.cv2_to_compressed_imgmsg(annotated_image)
                    # annotated_msg = self.cv_bridge.cv2_to_imgmsg(annotated_image, encoding="bgr8")
                    # annotated_msg.header = self.rgb_img.header
                    self.img_pub.publish(annotated_msg)
                    # print(aaa)
                    aaa += 1

                self.trans = self.tf_buffer.lookup_transform('map', 'head_rgbd_sensor_link', rospy.Time(0), rospy.Duration(1.0))
                _pose_stamped = PoseStamped()
                _pose_stamped.header.frame_id = 'map'

                neighborhood = 120

                nose = (results.pose_landmarks.landmark[mp_holistic.PoseLandmark.NOSE].x * width, results.pose_landmarks.landmark[mp_holistic.PoseLandmark.NOSE].y * height)
                # print("nose: ", nose)
                # nose_neighborhood = self.get_neighborhood_points(int(nose[0]), int(nose[1]))
                # # nose_3d = self.get_point(int(nose[0]), int(nose[1]))
                # min_distance_index = nose_neighborhood[2].index(min(nose_neighborhood[2]))
                # # print("nose_3d: ", nose_3d)
                # _pose_stamped.pose.position.x = nose_neighborhood[0][min_distance_index]
                # _pose_stamped.pose.position.y = nose_neighborhood[1][min_distance_index]
                # _pose_stamped.pose.position.z = nose_neighborhood[2][min_distance_index]
                # # tf
                # transformed = tf2_geometry_msgs.do_transform_pose(_pose_stamped, self.trans)
                # nose_map = [transformed.pose.position.x, transformed.pose.position.y, transformed.pose.position.z]
                nose_map = self.return_3dmap_frame(int(nose[0]), int(nose[1]))

                l_shoulder = (results.pose_landmarks.landmark[mp_holistic.PoseLandmark.LEFT_SHOULDER].x * width, results.pose_landmarks.landmark[mp_holistic.PoseLandmark.LEFT_SHOULDER].y * height)
                # # l_shoulder_3d = self.get_point(int(l_shoulder[0]), int(l_shoulder[1]))
                # l_shoulder_neighborhood = self.get_neighborhood_points(int(l_shoulder[0]), int(l_shoulder[1]))
                # min_distance_index = l_shoulder_neighborhood[2].index()
                # _pose_stamped.pose.position.x = l_shoulder_3d[0]
                # _pose_stamped.pose.position.y = l_shoulder_3d[1]
                # _pose_stamped.pose.position.z = l_shoulder_3d[2]
                # # tf
                # transformed = tf2_geometry_msgs.do_transform_pose(_pose_stamped, self.trans)
                # l_shoulder_map = [transformed.pose.position.x, transformed.pose.position.y, transformed.pose.position.z]
                l_shoulder_map = self.return_3dmap_frame(int(l_shoulder[0]), int(l_shoulder[1]))

                r_shoulder = (results.pose_landmarks.landmark[mp_holistic.PoseLandmark.RIGHT_SHOULDER].x * width, results.pose_landmarks.landmark[mp_holistic.PoseLandmark.RIGHT_SHOULDER].y * height)
                # r_shoulder_3d = self.get_point(int(r_shoulder[0]), int(r_shoulder[1]))
                # _pose_stamped.pose.position.x = r_shoulder_3d[0]
                # _pose_stamped.pose.position.y = r_shoulder_3d[1]
                # _pose_stamped.pose.position.z = r_shoulder_3d[2]
                # # tf
                # transformed = tf2_geometry_msgs.do_transform_pose(_pose_stamped, self.trans)
                # r_shoulder_map = [transformed.pose.position.x, transformed.pose.position.y, transformed.pose.position.z]
                r_shoulder_map = self.return_3dmap_frame(int(r_shoulder[0]), int(r_shoulder[1]))

                l_wrist = (results.pose_landmarks.landmark[mp_holistic.PoseLandmark.LEFT_WRIST].x * width, results.pose_landmarks.landmark[mp_holistic.PoseLandmark.LEFT_WRIST].y * height)
                # l_wrist_3d = self.get_point(int(l_wrist[0]), int(l_wrist[1]))
                # _pose_stamped.pose.position.x = l_wrist_3d[0]
                # _pose_stamped.pose.position.y = l_wrist_3d[1]
                # _pose_stamped.pose.position.z = l_wrist_3d[2]
                # # tf
                # transformed = tf2_geometry_msgs.do_transform_pose(_pose_stamped, self.trans)
                # l_wrist_map = [transformed.pose.position.x, transformed.pose.position.y, transformed.pose.position.z]
                l_wrist_map = self.return_3dmap_frame(int(l_wrist[0]), int(l_wrist[1]))

                r_wrist = (results.pose_landmarks.landmark[mp_holistic.PoseLandmark.RIGHT_WRIST].x * width, results.pose_landmarks.landmark[mp_holistic.PoseLandmark.RIGHT_WRIST].y * height)
                # r_wrist_3d = self.get_point(int(r_wrist[0]), int(r_wrist[1]))
                # _pose_stamped.pose.position.x = r_wrist_3d[0]
                # _pose_stamped.pose.position.y = r_wrist_3d[1]
                # _pose_stamped.pose.position.z = r_wrist_3d[2]
                # # tf
                # transformed = tf2_geometry_msgs.do_transform_pose(_pose_stamped, self.trans)
                # r_wrist_map = [transformed.pose.position.x, transformed.pose.position.y, transformed.pose.position.z]
                r_wrist_map = self.return_3dmap_frame(int(r_wrist[0]), int(r_wrist[1]))

                l_hip = (results.pose_landmarks.landmark[mp_holistic.PoseLandmark.LEFT_HIP].x * width, results.pose_landmarks.landmark[mp_holistic.PoseLandmark.LEFT_HIP].y * height)
                # l_hip_3d = self.get_point(int(l_hip[0]), int(l_hip[1]))
                # _pose_stamped.pose.position.x = l_hip_3d[0]
                # _pose_stamped.pose.position.y = l_hip_3d[1]
                # _pose_stamped.pose.position.z = l_hip_3d[2]
                # # tf
                # transformed = tf2_geometry_msgs.do_transform_pose(_pose_stamped, self.trans)
                # l_hip_map = [transformed.pose.position.x, transformed.pose.position.y, transformed.pose.position.z]
                l_hip_map = self.return_3dmap_frame(int(l_hip[0]), int(l_hip[1]))

                r_hip = (results.pose_landmarks.landmark[mp_holistic.PoseLandmark.RIGHT_HIP].x * width, results.pose_landmarks.landmark[mp_holistic.PoseLandmark.RIGHT_HIP].y * height)
                # r_hip_3d = self.get_point(int(r_hip[0]), int(r_hip[1]))
                # _pose_stamped.pose.position.x = r_hip_3d[0]
                # _pose_stamped.pose.position.y = r_hip_3d[1]
                # _pose_stamped.pose.position.z = r_hip_3d[2]
                # # tf
                # transformed = tf2_geometry_msgs.do_transform_pose(_pose_stamped, self.trans)
                # r_hip_map = [transformed.pose.position.x, transformed.pose.position.y, transformed.pose.position.z]
                r_hip_map = self.return_3dmap_frame(int(r_hip[0]), int(r_hip[1]))

                l_ankle = (results.pose_landmarks.landmark[mp_holistic.PoseLandmark.LEFT_ANKLE].x * width, results.pose_landmarks.landmark[mp_holistic.PoseLandmark.LEFT_ANKLE].y * height)
                # l_ankle_3d = self.get_point(int(l_ankle[0]), int(l_ankle[1]))
                # _pose_stamped.pose.position.x = l_ankle_3d[0]
                # _pose_stamped.pose.position.y = l_ankle_3d[1]
                # _pose_stamped.pose.position.z = l_ankle_3d[2]
                # # tf
                # transformed = tf2_geometry_msgs.do_transform_pose(_pose_stamped, self.trans)
                # l_ankle_map = [transformed.pose.position.x, transformed.pose.position.y, transformed.pose.position.z]
                l_ankle_map = self.return_3dmap_frame(int(l_ankle[0]), int(l_ankle[1]))

                r_ankle = (results.pose_landmarks.landmark[mp_holistic.PoseLandmark.RIGHT_ANKLE].x * width, results.pose_landmarks.landmark[mp_holistic.PoseLandmark.RIGHT_ANKLE].y * height)
                # r_ankle_3d = self.get_point(int(r_ankle[0]), int(r_ankle[1]))
                # _pose_stamped.pose.position.x = r_ankle_3d[0]
                # _pose_stamped.pose.position.y = r_ankle_3d[1]
                # _pose_stamped.pose.position.z = r_ankle_3d[2]
                # # tf
                # transformed = tf2_geometry_msgs.do_transform_pose(_pose_stamped, self.trans)
                # r_ankle_map = [transformed.pose.position.x, transformed.pose.position.y, transformed.pose.position.z]
                r_ankle_map = self.return_3dmap_frame(int(r_ankle[0]), int(r_ankle[1]))

                parts = [nose_map, l_shoulder_map, r_shoulder_map, l_wrist_map, r_wrist_map, l_hip_map, r_hip_map, l_ankle_map, r_ankle_map]
                center_shoulder_map = [(l_shoulder_map[0] + r_shoulder_map[0]) / 2, (l_shoulder_map[1] + r_shoulder_map[1]) / 2, (l_shoulder_map[2] + r_shoulder_map[2]) / 2]
                line = [[nose_map, center_shoulder_map], 
                        [l_shoulder_map, r_shoulder_map], 
                        [l_shoulder_map, l_wrist_map], 
                        [r_shoulder_map, r_wrist_map], 
                        [l_shoulder_map, l_hip_map], 
                        [r_shoulder_map, r_hip_map], 
                        [l_hip_map, r_hip_map], 
                        [l_hip_map, l_ankle_map], 
                        [r_hip_map, r_ankle_map]
                        ]
                # self.visualize_pose_landmark(parts)
                self.visualize_pose_line(line)
                # for i in range(len(parts)):
                #     print("parts: ", parts[i])
                #     if not(parts[i][2] is None) and not(math.isnan(parts[i][2]) is True):
                #         print(i)
                #         self.visualize_pose_landmark(parts[i])

            except Exception as e1:
                print(f"Unexpected {e1=}, {type(e1)=}")
                # return

    def visualize_pose_landmark(self, parts):
        self.poselandmark_marker_array_msg = MarkerArray()
        for i in range(len(parts)):
            # print("parts: ", parts[i])
            if not(parts[i][2] is None) and not(math.isnan(parts[i][2]) is True):
        # self.marker_msg = Marker()
        # for i in range(len(x_list)):
                marker = Marker()
                marker.header.frame_id = "map"
                marker.id = i
                # Object type, 矢印やったら0
                marker.type = 2
                marker.action = Marker.ADD
                marker.pose = Pose()
                marker.pose.position.x = parts[i][0]
                marker.pose.position.y = parts[i][1]
                marker.pose.position.z = parts[i][2]
                marker.pose.orientation.x = 0
                marker.pose.orientation.y = 0
                marker.pose.orientation.z = 0
                marker.pose.orientation.w = 0
                marker.color.r = 1.0
                marker.color.g = 0.0
                marker.color.b = 0.0
                marker.color.a = 1.0
                marker.scale.x = 0.1
                marker.scale.y = 0.1
                marker.scale.z = 0.1
                marker.frame_locked = False
                marker.lifetime = rospy.Duration(3)
                self.poselandmark_marker_array_msg.markers.append(marker)
        # marker.ns = "Goal-%u"%i
        # self.marker_msg.markers.append(marker)
        # self.marker_msg.lifetime = rospy.Duration(10)
        # self.point_pub.publish(self.marker_msg)
        self.keypoint_pub.publish(self.poselandmark_marker_array_msg)
        # self.rate.sleep()

    def visualize_pose_line(self, line):
        self.landmarkline_marker_array_msg = MarkerArray()
        for i in range(len(line)):
            if not(line[i][0][2] is None) and not(math.isnan(line[i][0][2]) is True) and not(line[i][1][2] is None) and not(math.isnan(line[i][1][2])):
            # print(1)
                marker = Marker()
                start_point = Point()
                start_point.x = line[i][0][0]
                start_point.y = line[i][0][1]
                start_point.z = line[i][0][2]
                end_point = Point()
                end_point.x = line[i][1][0]
                end_point.y = line[i][1][1]
                end_point.z = line[i][1][2]

                marker.header.frame_id = "map"
                marker.id = i
                marker.type = 0
                marker.action = Marker.ADD
                # marker.points.resize(2)
                # marker.pose = reshape()
                marker.points.append(start_point)
                marker.points.append(end_point)
                # marker.color.r = i / 10
                # marker.color.g = 1.0 - i / 10
                # marker.color.b = 0.0
                marker.color.r = 1.0
                marker.color.g = 0.0
                marker.color.b = 0.0
                marker.color.a = 0.8
                marker.scale.x = 0.06
                marker.scale.y = 0.0
                marker.scale.z = 0.0
                marker.frame_locked = False
                marker.lifetime = rospy.Duration(3)
                self.landmarkline_marker_array_msg.markers.append(marker)

        self.landmark_line_pub.publish(self.landmarkline_marker_array_msg)

    def return_3dmap_frame(self, cx, cy):
        self.trans = self.tf_buffer.lookup_transform('map', 'head_rgbd_sensor_link', rospy.Time(0), rospy.Duration(1.0))
        _pose_stamped = PoseStamped()
        _pose_stamped.header.frame_id = 'map'

        neighborhood_number = 24

        parts_neighborhood = self.get_neighborhood_points(int(cx), int(cy), neighborhood_number)
        # nose_3d = self.get_point(int(nose[0]), int(nose[1]))
        min_distance_index = parts_neighborhood[2].index(min(parts_neighborhood[2]))
        # print("nose_3d: ", nose_3d)
        _pose_stamped.pose.position.x = parts_neighborhood[0][min_distance_index]
        _pose_stamped.pose.position.y = parts_neighborhood[1][min_distance_index]
        _pose_stamped.pose.position.z = parts_neighborhood[2][min_distance_index]
        # tf
        transformed = tf2_geometry_msgs.do_transform_pose(_pose_stamped, self.trans)
        parts_map = [transformed.pose.position.x, transformed.pose.position.y, transformed.pose.position.z]
        
        return parts_map

    def get_neighborhood_points(self, cx, cy, value):
        """
        画像座標系
         u →
        v
        ↓
        """
        neighborhood_points = [[], [], []]
        width = int(math.sqrt(value + 1))
        for i in range(value + 1):
            for j in range(width):  # v
                for k in range(width):  # u
                    if j + 1 == k + 1 == math.ceil(width + 1):  # 中心点の判定
                        continue
                    else:

                        # uの取得
                        u = cx - (math.floor(width / 2)) + k

                        # vの取得
                        v = cy - (math.floor(width / 2)) + j

                        # カメラ座標の値を取得
                        position = self.get_point(int(u), int(v)).tolist()
                        if position is False:
                            return False

                        for l in range(len(neighborhood_points)):
                            neighborhood_points[l].append(position[l])
        return neighborhood_points


    def image_callback(self, message):
        # rospy.loginfo("Entered callback")
        self.rgb_img = self.cv_bridge.compressed_imgmsg_to_cv2(message)

    def point_cloud_callback(self, point_cloud):
        # print("aaaaaaaaaaaaaaaaaa")
        self._point_cloud = pointcloud2_to_xyz_array(point_cloud, False)
        self._point_cloud_header = point_cloud.header

    def get_point(self, x, y):
        try:
            # print("get_point_1: ", self._point_cloud[y][x])
            return self._point_cloud[y][x]
        except:
            # print("get_point_2: ", self._point_cloud[y][x])
            pass


if __name__ == '__main__':
    rospy.init_node('output_pose_landmark')
    node = OutputPoseLandmark()
    node.output_pose_landmark()
    rospy.spin()