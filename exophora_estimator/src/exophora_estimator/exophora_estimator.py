#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ROS
import rospy
from sensor_msgs.msg import CompressedImage, Image

import rospy
import os
import csv
import tf
from tf import TransformListener
import tf2_ros
import tf2_geometry_msgs
from std_msgs.msg import String, Header
from geometry_msgs.msg import Pose, PoseStamped
from sensor_msgs.msg import PointCloud2
from sensor_msgs.msg import CompressedImage, Image
import cv2
from cv_bridge import CvBridge
import time
import numpy as np
import statistics
import math
from visualization_msgs.msg import Marker, MarkerArray
# 正規分布
from scipy.stats import multivariate_normal
# フォンミーゼス分布
from scipy.stats import vonmises

from PIL import Image
import matplotlib.pyplot as plt

# 下4つはmediapipe用
import mediapipe as mp
mp_holistic = mp.solutions.holistic
mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose

from ros_numpy.point_cloud2 import pointcloud2_to_xyz_array
# ガウス分布可視化のためのもの
import rviz_gaussian_distribution_msgs.msg as rgd_msgs
from matplotlib import colors

# Standard
import numpy as np
import yaml
# from PIL import Image
# import matplotlib.pyplot as plt

# _point_cloud_header = Header()
# tf_buffer = tf2_ros.Buffer()
# # これコメントアウトされてなかった
# tf_listener = tf2_ros.TransformListener(tf_buffer)


# Self
from .modules import (
    calculate_pointing_vector,
    dataset,
    demonstrative_region_estimator,
    get_pose_landmark,
    pointing_estimator,
    visualize_pointing_trajectory
)

# self._calculate_pointing_func = calculate_pointing_vector.CalculatePointingVector()
# self._dataset_func = dataset.Dataset()
# self._demonstrative_estimator_func = demonstrative_region_estimator.DemonstrativeRegionEstimator()
# self._get_pose_landmark_func = get_pose_landmark.GetPoseLandmark()
# pointing_estimator_func = pointing_estimator.PointingEstimator()
# self._visualize_pointing_func = visualize_pointing_trajectory.VisualizePointingTrajectory()


class ExophoraEstimator():
    def __init__(self):

        _point_cloud_header = Header()
        tf_buffer = tf2_ros.Buffer()
        # これコメントアウトされてなかった
        tf_listener = tf2_ros.TransformListener(tf_buffer)

        self._calculate_pointing_func = calculate_pointing_vector.CalculatePointingVector()
        self._dataset_func = dataset.Dataset()
        self._demonstrative_estimator_func = demonstrative_region_estimator.DemonstrativeRegionEstimator()
        self._get_pose_landmark_func = get_pose_landmark.GetPoseLandmark()
        self._pointing_estimator_func = pointing_estimator.PointingEstimator()
        self._visualize_pointing_func = visualize_pointing_trajectory.VisualizePointingTrajectory()

        self.object_category = ["Bottle", "Stuffed Toy", "Book", "Cup"]
        self.target_object_name = "Bottle"

        with open('yolov5m_Object365.yaml', 'r') as yml:
            object_yaml = yaml.load(yml, Loader=yaml.SafeLoader)
            self.object_365 = object_yaml['names']

        self.kosoa = "a"
        # self.sub = rospy.Subscriber("/hsrb/head_rgbd_sensor/rgb/image_rect_color/compressed", CompressedImage, self.main)
        self.img_pub = rospy.Publisher("/annotated_msg/compressed", CompressedImage, queue_size=1)


    ### non_realtimeとrealtimeで存在する関数と存在しないものがあるため、両方を参照しながらプログラムを作成
    # 基本的には、
    def main(self):
        # 物体のインデックスとカテゴリと3次元座標のリスト作成
        # リストの中身は [object number, object name, x, y, z] が20個
        object_list = self._dataset_func.load_object_position_data(self.object_category)

        # リストに6個の座標が格納されたらループを抜ける
        # num = 0
        # while num < 7:
        #     # eye_x, eye_y, eye_z, wrist_x, wrist_y, wrist_z = self._get_pose_landmark_func.landmark_frame()
        #     wrist_x, wrist_y, wrist_z, eye_x, eye_y, eye_z = self._get_pose_landmark_func.main()
        #     num = len(eye_x)

        wrist_x, wrist_y, wrist_z, eye_x, eye_y, eye_z = self._get_pose_landmark_func.main()

        rospy.loginfo("Skeleton could be detected!")

        # 指差しベクトルの計算と可視化
        point_ground, param, wrist_frame, eye_frame = self._calculate_pointing_func.calculate_pointing_vector(wrist_x, wrist_y, wrist_z, eye_x, eye_y, eye_z)
        self._visualize_pointing_func.visualize_point(point_ground)
        self._visualize_pointing_func.visualize_eye(eye_frame)
        self._visualize_pointing_func.visualize_pointing(param, wrist_frame, eye_frame)

        # フォン・ミーゼス分布を用いた指差し方向に基づく確率推定器により、対象確率の出力
        # for i in range(len(object_position)):
        pointing_prob = self._pointing_estimator_func.pointing_inner_product(object_list, wrist_frame, eye_frame)

        # 指示語領域に基づく確率推定器により、対象確率の出力
        demonstrative_prob = self._demonstrative_estimator_func.calculate_demonstrative_region(object_list, wrist_frame, eye_frame, self.kosoa)
        # self.visualize_kosoa()

        # 物体カテゴリの信頼度スコアを取得 (物体カテゴリに基づく確率推定器)
        self.target_object_id = self.object_365.index(self.target_object_name)
        object_class_prob = self._dataset_func.load_object_confidence(self.object_category, self.target_object_id)

        demonstrative_prob = np.array(demonstrative_prob)
        pointing_prob = np.array(pointing_prob)


        # 3つの確率値を掛け合わせる
        target_probability = object_class_prob * demonstrative_prob * pointing_prob
        # target_probability = demonstrative_prob * pointing_prob
        sum_target_probability = np.sum(target_probability)

        # 正規化
        target_probability = target_probability / sum_target_probability
        target = np.amax(target_probability)
        target_index = np.argmax(target_probability)

        # print用に小数点第3位を四捨五入
        target_probability = np.round(target_probability, decimals=2)
        # print(object_list)
        target_probability_reshape = np.reshape(target_probability, (4, 5))

        k = 0
        for i in range(4):
            print("対象確率", target_probability[k:k + 5])
            k += 5
        print("予測した目標物体", object_list[target_index])
        # demonstrative_p = np.round(demonstrative_p, decimals=2)
        # print(demonstrative_p)


if __name__ == '__main__':
    rospy.init_node('exophora_estimator')
    node = ExophoraEstimator()
    node.main()
    # rospy.spin()

